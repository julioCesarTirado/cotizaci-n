package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import org.w3c.dom.Text;

public class CotizacionActivity extends AppCompatActivity {
    private TextView txtfolio;
    private TextView txtCliente;
    private TextView txtDescripcion;
    private TextView txtValorAuto;

    private TextView txtPagoInicial;
    private RadioButton rdb12;
    private RadioButton rdb24;
    private RadioButton rdb36;
    private RadioButton rdb48;
    private Button btnRegresar;
    private Button btnLimpiar;
    private Button btnCalcular;
    private TextView lblPagoMensual;
    private TextView lblPagoInicial;

    private Cotizacion cotiza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        txtfolio = (TextView) findViewById(R.id.txtfolio);
        txtCliente = (TextView) findViewById(R.id.txtCliente2);
        final TextView txtDescripcion = (TextView) findViewById(R.id.txtDescripcion);
        final TextView txtValorAuto = (TextView) findViewById(R.id.txtValorAuto);
        final TextView txtPagoInicial = (TextView) findViewById(R.id.txtPagoInicial);
        final RadioButton rdb12 = (RadioButton) findViewById(R.id.rdb12);
        final RadioButton rdb24 = (RadioButton) findViewById(R.id.rdb24);
        final RadioButton rdb36 = (RadioButton) findViewById(R.id.rdb36);
        final RadioButton rdb48 = (RadioButton) findViewById(R.id.rdb48);
        final Button btnRegresar = (Button) findViewById(R.id.btnRegresar);
        final Button btnLimpiar= (Button) findViewById(R.id.btnLimpiar);
        Button btnCalcular = (Button) findViewById(R.id.btnCalcular);
        final TextView lblPagoInicial = (TextView) findViewById(R.id.lblPagoInicial);
        final TextView lblPagoMensual = (TextView) findViewById(R.id.lblPagoMensual);


        Bundle datos = getIntent().getExtras();
        txtCliente.setText(datos.getString("Cliente"));

        cotiza = (Cotizacion) datos.getSerializable("cotizacion");
        txtfolio.setText(String.valueOf("Folio: " + cotiza.getFolio()));


        btnCalcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (txtDescripcion.getText().toString().matches("") ||
                txtValorAuto.getText().toString().matches("") ||
                txtPagoInicial.getText().toString().matches("")){
                    Toast.makeText(CotizacionActivity.this, "Faltó Capturar Información",
                            Toast.LENGTH_SHORT).show();
                    txtDescripcion.requestFocus();
                } else{
                    int plazos = 0;
                    float enganche = 0;
                    float pagomensual = 0.0f;

                    if (rdb12.isChecked()){plazos = 12;}
                    if (rdb24.isChecked()){plazos = 24;}
                    if (rdb36.isChecked()) {plazos = 36;}
                    if (rdb48.isChecked()){plazos = 48;}

                    cotiza.setDescripcion(txtDescripcion.getText().toString());
                    cotiza.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cotiza.setPorEnganche(Float.parseFloat(txtPagoInicial.getText().toString()));
                    cotiza.setPlazos(plazos);

                    enganche = cotiza.calcularPagoInicial();
                    pagomensual = cotiza.calcularPagoMensual();

                    lblPagoInicial.setText("Pago Inicial $" + String.valueOf(enganche));
                    lblPagoMensual.setText("Pago Mensual $" + String.valueOf(pagomensual));


                }

                btnLimpiar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lblPagoInicial.setText("");
                        txtDescripcion.setText("");
                        lblPagoMensual.setText("");
                        txtPagoInicial.setText("");
                        txtValorAuto.setText("");
                        rdb12.setSelected(true);
                    }
                });

            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
