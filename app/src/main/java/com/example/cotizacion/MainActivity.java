package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Cotizacion cotiza = new Cotizacion();
    private EditText txtCliente;
    private Button btnIngresar;
    private Button btnSalir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtCliente = (EditText) findViewById(R.id.txtcliente);
        btnIngresar = (Button) findViewById(R.id.btningresar);
        btnSalir = (Button) findViewById(R.id.btnterminar);


        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtCliente.getText().toString();
                if (cliente.matches("")){
                    Toast.makeText(MainActivity.this,
                            "Falto Capturar Nombre",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this,
                            CotizacionActivity.class);
                    intent.putExtra("Cliente",cliente);

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("cotizacion",cotiza);

                    intent.putExtras(objeto);

                    startActivity(intent);

                }

            }


        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
